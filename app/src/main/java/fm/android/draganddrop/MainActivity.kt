package fm.android.draganddrop

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import java.util.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupView()
    }

    private fun setupView() {
        val recyclerView : RecyclerView = findViewById(R.id.rv_colors)
        val layoutManager = GridLayoutManager(this, 2)

        val adapter = ColorsAdapter(this, getColors())


        val itemTouchHelper = ItemTouchHelper(DragAndDropTouchCallback(adapter))
        itemTouchHelper.attachToRecyclerView(recyclerView)


        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager
    }

    private fun getColors() = listOf(
        Color.BLUE,
        Color.GRAY,
        Color.YELLOW,
        Color.GREEN,
        Color.MAGENTA,
        Color.RED,
        Color.BLACK,
        Color.LTGRAY,
    )
}